<?php
$this->breadcrumbs=array(
	$model->firstName => $model->url,
	'Изменение записи',
);
?>

<h1>Изменение записи <i><?php echo CHtml::encode($model->firstName); ?></i></h1>

<?php echo $this->renderPartial('_form', array('model' => $model)); ?>