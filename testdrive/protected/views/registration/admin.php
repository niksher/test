<?php
$this->breadcrumbs=array(
	'Управление записями',
);
?>
<h1>Управление записями</h1>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'dataProvider' => $model->search(),
	'filter' => $model,
	'columns' => array(
		array(
			'name' => 'firstName',
			'type' => 'raw',
			'value' => 'CHtml::link(CHtml::encode($data->firstName), $data->url)'
		),
    array(
			'name'=>'lastName',
      'type' => 'raw',
			'filter'=>false,
		),
		array(
			'name' => 'state',
			'value' => '$data->state==1?"Опубликовано":"В архиве"',
			'filter' => false,
		),
		
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
