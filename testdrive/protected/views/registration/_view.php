<div class="post">
	<div class="field">
		Имя: <?php echo CHtml::link(CHtml::encode($data->firstName), $data->url); ?>
	</div>
	<div class="field">
		Фамилия: <?php echo CHtml::encode($data->lastName); ?>
	</div>
  
  <div class="field">
		Телефон: <?php echo CHtml::encode($data->phone); ?>
	</div>
  <div class="field">
		Email: <?php echo CHtml::encode($data->email); ?>
	</div>
  <div class="field">
		Дата визита: <?php echo CHtml::encode($data->visitDate); ?>
	</div>
  <div class="field">
		Время визита: <?php echo CHtml::encode($data->visitTime); ?>
	</div>
    
  <div class="field">
		Статус: <?php echo CHtml::encode($data->state?"Опубликовано":"В архиве"); ?>
	</div>

</div>
