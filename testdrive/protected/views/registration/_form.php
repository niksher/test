<div class="form">

<?php $form = $this->beginWidget('CActiveForm'); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo CHtml::errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model, 'First Name'); ?>
		<?php echo $form->textField($model, 'firstName', array('size' => 80, 'maxlength' => 128)); ?>
		<?php echo $form->error($model, 'firstName'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model, 'Last Name'); ?>
		<?php echo $form->textField($model, 'lastName', array('size' => 80, 'cols' => 128)); ?>
		<?php echo $form->error($model, 'lastName'); ?>
	</div>
  
  <div class="row">
		<?php echo $form->labelEx($model, 'Phone Number'); ?>
		<?php echo $form->textField($model, 'phone', array('size' => 80, 'cols' => 128)); ?>
		<?php echo $form->error($model, 'phone'); ?>
	</div>
  
  <div class="row">
		<?php echo $form->labelEx($model, 'Email'); ?>
		<?php echo $form->textField($model, 'email', array('size' => 80, 'cols' => 128)); ?>
		<?php echo $form->error($model, 'email'); ?>
	</div>
  
  <div class="row">
		<?php echo $form->labelEx($model, 'Date Visit d/m/y'); ?>
		<?php echo $form->textField($model, 'visitDate', array('size' => 80, 'cols' => 128)); ?>
		<?php echo $form->error($model, 'visitDate'); ?>
	</div>
  
  <div class="row">
		<?php echo $form->labelEx($model, 'Time visit h:i'); ?>
		<?php echo $form->textField($model, 'visitTime', array('size' => 80, 'cols' => 128)); ?>
		<?php echo $form->error($model, 'visitTime'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model, 'Status'); ?>
		<?php echo $form->dropDownList($model, 'state', [1 => "Опубликовано", 2 => "В архиве"]); ?>
		<?php echo $form->error($model, 'state'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->