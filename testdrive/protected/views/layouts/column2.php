<?php $this->beginContent('/layouts/main'); ?>
<div class="container">
	<div class="span-18">
		<div id="content">
			<?php echo $content; ?>
		</div><!-- content -->
	</div>
	<div class="span-6 last">
		<div id="sidebar">
      <?php 
        if (!Yii::app()->user->isGuest){
          $this->widget('UserMenu');
        } else {
          echo "<ul><li>" . CHtml::link('Добавить запись', array('registration/create')) . "</li></ul>";
        }
      ?>
		</div>
	</div>
</div>
<?php $this->endContent(); 