<?php

/**
 * The followings are the available columns in table 'registration':
 * @property integer $id
 * @property string $firstName
 * @property string $lastName
 * @property string $phone
 * @property integer $email
 * @property string $visitDate
 * @property integer $state
 * @property string $visitTime
 */
class Registration extends CActiveRecord
{
	const STATUS_PUBLISHED = 1;
	const STATUS_ARCHIVED = 2;
  
  private $err = false;

	/**
	 * Returns the static model of the specified AR class.
	 * @return static the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'registration';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('firstName, lastName', 'required'),
      array('phone', 'numerical'),
      array('email', 'email'),
      array('visitDate', 'date', 'format' => "d/m/y"),
      array('visitTime', 'date', 'format' => "h:m"),
      array('state', 'in', 'range' => array(1,2)),
      array('phone, email' , 'customChecker'),
			
			array('firstName, lastName, email, phone, visitDate, visitTime', 'safe', 'on'=>'search'),
      array('firstName, lastName, email, phone, visitDate, visitTime', 'safe'),
		);
	}
  
  public function customChecker($attribute, $params) {
    if (($attribute == "phone") && $this->$attribute == "") {
      $this->err = true;
    }
    if (($attribute == "email") && $this->$attribute == "" && $this->err) {
      $this->addError($attribute, 'Поле email или phone должно быть заполнено');
      $this->addError("phone", 'Поле email или phone должно быть заполнено');
    }
  }

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array();
	}

	/**
	 * @return array customized attribute labels (name => label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'Id',
			'firstName' => 'firstName',
			'lastName' => 'lastName',
			'phone' => 'phone',
			'email' => 'email',
			'visitDate' => 'visitDate',
      'visitTime' => 'visitTime',
			'state' => 'state',
		);
	}

	/**
	 * @return string the URL that shows the detail of the post
	 */
	public function getUrl()
	{
		return Yii::app()->createUrl('registration/view', array(
			'id'=>$this->id,
		));
	}
  
  /**
	 * This is invoked after the record is saved.
	 */
	protected function afterSave()
	{
		parent::afterSave();
	}
  
  	/**
	 * Retrieves the list of posts based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the needed posts.
	 */
	public function search()
	{
		$criteria = new CDbCriteria;

		$criteria->compare('firstName', $this->firstName, true);

		$criteria->compare('state', $this->state);

		return new CActiveDataProvider('Registration', array(
			'criteria' => $criteria,
		));
	}
}